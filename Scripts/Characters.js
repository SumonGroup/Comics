
var currentPage = GetQueryStringByName("page");
if (currentPage == null || currentPage == "") {
    currentPage = 1;
}
var offset = (currentPage - 1) * _pageSize;

FetchListOfCharacters(GenerateApiURL(_dataTypes.characters) + "&offset=" + offset);

function FetchListOfCharacters(requestUrl) {
    var XHR = new HttpRequest(requestUrl, null);
    XHR.executeRequest(function (result) {
        LoadCharacters(result.data.results);
        LoadPaging(currentPage, _pageSize, result.data.total);
    });
}

function LoadCharacters(liCharacters) {
    var length = liCharacters.length;

    for (var i = 0; i < length; i++) {
        GetBriefLayoutForCharacter(liCharacters[i]).then(sectionElement => {
            AddNewElement(sectionElement, "charactersContainer");
        });
    }
}

function GetBriefLayoutForCharacter(character) {
    var promise = new Promise(function (resolve, reject) {
        resolve(PrepareBriefLayout(character));
    });
    return promise;
}

function LoadPaging(currentPage, pagesize, totalRecord) {
    AddValue(currentPage, 'txtPage');
    var totalPage = Math.floor(totalRecord / pagesize) + (totalRecord % pagesize);
    AddInnerHTML(totalPage, 'lblTotalPage');
}

