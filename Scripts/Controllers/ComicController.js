﻿

function GetDetailLayoutForComic(comic) {
    var promise = new Promise(function (resolve, reject) {
        var sectionElement = PrepareDetailLayout(comic);
        resolve(sectionElement);
    });
    return promise;
}

function GetBriefLayoutForComic(comic) {
    var promise = new Promise(function (resolve, reject) {
        resolve(PrepareBriefLayout(comic));
    });
    return promise;
}

function PrepareBriefLayout(comic) {
    var sectionElement = document.createElement("section");
    sectionElement.setAttribute("class", "inner-wrapper");

    var articleElement = document.createElement("article");

    var imgElement = document.createElement("img");
    imgElement.setAttribute('src', comic.thumbnail.path + "/portrait_xlarge." + comic.thumbnail.extension);

    var h3ElementTitle = document.createElement("h4");
    h3ElementTitle.innerHTML = comic.title;

    articleElement.appendChild(imgElement);
    articleElement.appendChild(h3ElementTitle);

    var aElementDetails = document.createElement("a");
    aElementDetails.setAttribute('href', "ComicDetails.html?id=" + comic.id);
    aElementDetails.appendChild(articleElement);
    sectionElement.appendChild(aElementDetails);
    return sectionElement;
}

function PrepareDetailLayout(comic) {
    var sectionElement = document.createElement("section");
    sectionElement.setAttribute("class", "inner-wrapper");

    var articleElement = document.createElement("article");
    var asideElement = document.createElement("aside");

    var imgElement = document.createElement("img");
    imgElement.setAttribute('src', comic.thumbnail.path + "/portrait_uncanny." + comic.thumbnail.extension);

    var h3Element = document.createElement("h3");
    h3Element.innerHTML = comic.title;

    var pElementDesc = document.createElement("p");
    pElementDesc.innerHTML = comic.description;

    var pElementSeries = document.createElement("p");
    pElementSeries.innerHTML = "Series: " + comic.series.name;

    var pElementCharacters = document.createElement("p");
    var characterList = "Characters:";

    if (comic.characters.available > 0) {
        for (var i = 0; i < comic.characters.available; i++) {
            var characterId = comic.characters.items[i].resourceURI.slice(comic.characters.items[i].resourceURI.lastIndexOf("/") + 1)
            characterList += "&nbsp<a href='CharacterDetails.html?id=" + characterId + "'>" + comic.characters.items[i].name + "</a>";
        }
    }
    else {
        characterList += " No character found.";
    }
    pElementCharacters.innerHTML = characterList;

    articleElement.appendChild(imgElement);

    asideElement.appendChild(h3Element);
    asideElement.appendChild(pElementDesc);
    asideElement.appendChild(pElementSeries);
    asideElement.appendChild(pElementCharacters);

    sectionElement.appendChild(articleElement);
    sectionElement.appendChild(asideElement);
    return sectionElement;
}
