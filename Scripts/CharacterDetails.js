﻿
FetchCharacterDetails(_baseAPIUrl + "characters/" + GetQueryStringByName("id") + "?" + _apiKey);

function FetchCharacterDetails(requestUrl) {
    var XHR = new HttpRequest(requestUrl, null);
    XHR.executeRequest(function (result) {
        GetDetailLayoutForCharacter(result.data.results[0]).then(sectionElement => {
            AddNewElement(sectionElement, "characterContainer");
        });
    });
}